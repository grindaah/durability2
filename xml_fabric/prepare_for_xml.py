import argparse
from sys import argv

parser = argparse.ArgumentParser(description = "preparation of XMLs")
parser.add_argument('--out', '-o', default="output_file", help="name of the output", dest="output_file")
parser.add_argument('--in', '-i', default='bir1.proto', help="name of the input file", dest="input_file")
parser.add_argument('--frame_no', '-f', default="23", help="number of frame", dest="frame_number")
parser.add_argument('--board', '-b', choices = ['l', 'r'], default='l', help="left or right board", dest="board")

def get_args(argv):
    return parser.parse_args()

def get_next(el, cont):
    try:
        return cont[cont.index(el) + 1]
    except ValueError:
        return None

def main(args):
    for line in open(args.input_file):
        elems = line.split('\t')
        result_elems = []
        if elems[0].count('_') > 0:
            for elem in elems:
                split_elem = elem.split('_')

                if len(split_elem[1]) == 1:
                    if split_elem[1] == '1':
                        next_elem = get_next(elem, elems)
                        print next_elem.split('_')[1].count('1')
                        if next_elem != None and next_elem.split('_')[1].count('1') != 1:
                            print next_elem
                            split_elem[1] = "0%s" % split_elem[1]
                    else:
                        split_elem[1] = "0%s" % split_elem[1]

                result_elem = "ref_%s_%s_%s_%s" % (args.board, args.frame_number, split_elem[0], split_elem[1])
                result_elems.append(result_elem)
            result_str = " ".join(result_elems)
        elif elems[0].count(',') > 0:
            result_str = ".".join(line.split(','))
        else:
            result_str = line

        open(args.output_file, 'aw').writelines(result_str)
        print result_str

if __name__ == "__main__":
    args = parser.parse_args()
    main(args)
