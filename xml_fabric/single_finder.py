#coding: utf-8
import os


def check_xml_file(filename):
    if filename[-4:] == ".xml":
        return True
    else:
        return False

def find_singles(str):
    i = 1
    while i != 0:
        try:
            i = str.lower().index("ids")
        except ValueError:
            break
        counter += 1
        str = str[i+6:]

    return counter

def rebuild_ids_line(ids):
    ids_str = ' '.join(ids)
    ln = '    <ids value="%s></ids>' % ids_str
    return ln

all_files_in_dir = os.listdir('.')
xml_files = [fl for fl in all_files_in_dir if check_xml_file(fl)]

for fl in xml_files:
    changes_made = False
    f = open(fl)
    fl2 = fl + ".new"
    f_new = open(fl2, 'w')
    print fl
    print '=' * 60
    for line in f:
        if line.count('ids value="') > 0:
            only_ids = line[line.find('ref'):-8]
            ids = only_ids.split(' ')
            for id in ids:
                if ids.index(id) < len(ids):
                    if id[-2:] == "01" and (ids[ids.index(id)+1][-2:] == "01"):
                        print ids.index(id)
                        ids[ids.index(id)] = id[:-3]
                        line = rebuild_ids_line(ids)
                        changes_made = True

                else:
                    if id[-2:] == "01":
                        print id
                        ids[ids.index(id)] = id[:-3]
                        line = rebuild_ids_line(ids)
                        changes_made = True

        f_new.write(line)

    if changes_made:
        print rebuild_ids_line(ids)
    else:
        os.remove(fl2)
