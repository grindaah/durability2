#ifndef CONSTANTS_HOLDER_HPP
#  define CONSTANTS_HOLDER_HPP

#  include <iostream>
#  include <vector>
#  include <map>
#  include <string>
#  include <sstream>
#  include <cassert>
#  include <cmath>
#  include <stdlib.h>

#  include "../pugixml/pugixml.hpp"
#  include "../maintask/d_exceptions.hpp"

namespace cnst
{
    const double Lk = 86.28;
    const double Bk = 15.52;                               //ширина корпуса
    const double alpha     = 0.79;
    const double V         = 0.514*16.;
    const double T = 4.96;                                 // осадка
    const double D = 3926.;
    const double M_t = 7480.;
    const double delta = 0.5;
    const double g = 9.81;
    const double P = 100.0;

    const double F_rt = V/sqrt(g*Lk);

    const double n_v1 = 0.542;
    const double n_v2 = 0.857;

    const double F_rv1 = F_rt*n_v1;
    const double F_rv2 = F_rt*n_v2;

    const double sigma_0 = .836 * sqrt(Lk/T);              // собственная частота килевой качки корабля

    const double lambda_dash_1 = 1.093;
    const double lambda_dash_2 = 1.22;

    const double lambda_1 = lambda_dash_1 * Lk;
    const double lambda_2 = lambda_dash_2 * Lk;

    //высота расчетной волны
    const double h_1 = 13.728;
    const double h_2 = 7.515;

    const double k_pv = 0.035;
    const double k_vv = 0.0302;
    const double k_11 = 1.269;
    const double k_12 = 1.47;
    const double k_2  = 0.578;

    ///величины изгибающих моментов
    const double M_h1_vv =  (h_1 / 2) * k_vv * k_11 * k_2 * Bk * (Lk * Lk);
    const double M_h1_pv = (-h_1 / 2) * k_pv * k_11 * k_2 * Bk * Lk * Lk;
    const double M_h2_vv = (-h_2 / 2) * k_vv * k_12 * k_2 * Bk * Lk * Lk;
    const double M_h2_pv = (-h_2 / 2) * k_pv * k_12 * k_2 * Bk * Lk * Lk;

    ///величины волновых составляющих перерезывающих моментов
    const double Q_h1_vv = 3.5 * M_h1_vv / Lk;
    const double Q_h1_pv = 3.5 * M_h1_pv / Lk;
    const double Q_h2_vv = 3.5 * M_h2_vv / Lk;
    const double Q_h2_pv = 3.5 * M_h2_pv / Lk;
}

/**
 * \class constants_holder
 * \brief should get and store data from sequences i.e
 *        should store const vectors
 *
 * \todo  all asserts should be replaced with exceptions
 * \todo  second constructor with name
 */
template <typename T>
class constants_holder
{
    std::vector<T> m_cnst;
    std::string m_name;

    bool single_type;

public:
    typedef typename std::vector<T>::iterator iterator_type;
    typedef typename std::vector<T>::const_iterator const_iterator_type;

    ///constructor for single constants
    constants_holder() :
                single_type(false)
    {
    }

    ///constructor for single constants
    constants_holder(const T& elem) :
                single_type(true)
    {
        m_cnst.push_back(elem);
    }

    /// input operator
    void operator >>(const char* sequence)
    {
        T elem;
        char* mutable_sequence = const_cast<char *>(sequence);
        std::stringstream str(mutable_sequence);
        if (!single_type)
        {
            while (str >> elem)
                m_cnst.push_back(elem);
        }
        else
        {
            m_cnst.resize(1);
            m_cnst.reserve(1);
            str >> m_cnst[0];
        }
    }

    void append(const T& elem)
    {
        if (!single_type)
            m_cnst.push_back(elem);
    }

    operator T() const
    {
        if (m_cnst.empty())
            ex_thrower();
        return m_cnst.at(0);
    }

    const std::vector<T>& get_vector() const
    {
        return m_cnst;
    }

    ///\note use operator [0] when work with single constant
    T operator[](size_t index) const
    {
        if (m_cnst.size() > index)
            return m_cnst[index];
        else
            ex_thrower();
        return m_cnst[index];
    }

    T at(size_t index)
    {
        if (m_cnst.size() > index)
            return m_cnst.at(index);
        else
            ex_thrower();
        return m_cnst[index];
    }

    friend std::ostream& operator <<(std::ostream& str, const constants_holder& rhv)
    {
        if (rhv.m_name.size())
            str << "Constant " << rhv.m_name << ": ";
        else
            str << "Unnamed constant: ";
        str << ", size: " << rhv.m_cnst.size()
            << ", type: " << rhv.single_type << std::endl;
        for(const_iterator_type it = rhv.m_cnst.begin(); it != rhv.m_cnst.end(); ++it)
            str << *it << " ";
        return str << std::endl;
    }

    /// we will need size when constructing matrixes
    size_t size() const
    {
        return m_cnst.size();
    }

    void flush()
    {
        m_cnst.erase(m_cnst.begin(), m_cnst.end());
        assert(m_cnst.size() == 0 && "wtf? size should be 0!");
    }

    ///\note use when assume, this is single constant
    T get() const
    {
        if (m_cnst.empty())
            ex_thrower();
        return m_cnst.at(0);
    }

    void setname(const char* nm)
    {
        m_name = nm;
    }

    std::string getname() const
    {
        return m_name;
    }

    void ex_thrower() const
    {
        d_exception ex(d_exception::invalid_constant_access);
        ex.addition = m_name;
        throw ex;
    }
};

/**
 * @brief The var_base struct
 * @todo  should be template also
 *
 */
struct var_base
{
    std::map<std::string, constants_holder<double> > var_map;
    typedef std::map<std::string, constants_holder<double> >::iterator var_map_iterator;
    typedef std::map<std::string, constants_holder<double> >::const_iterator const_var_map_iterator;

    var_base() :
        xml_data(false)
    {
    }

    void append_element(const std::string str)
    {
        var_map[str];
    }

    void append_element(const std::string str, double val)
    {
        var_map[str] = val;
    }

    void merge(const var_base& v)
    {
        var_map.insert(v.var_map.begin(), v.var_map.end());
    }

    void overwrite(const var_base& v)
    {
        //var_map.insert(v.var_map.begin(), v.var_map.end());
        for (const_var_map_iterator it = v.var_map.begin()
                                      ; it != v.var_map.end()
                                      ; ++it
                                      )
            var_map[it->first] = it->second;
    }

    bool get_from_xml(const char* file_name)
    {
        pugi::xml_document doc;
        pugi::xml_parse_result result = doc.load_file(file_name);

        if (result)
        {
            std::cout << "xml readed" << std::endl;
            pugi::xml_node tools = doc.root().first_child();
            for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
            {
                var_map_iterator it = var_map.find( std::string(tool.name()));
                //std::cout << "searching: " << tool.name() << std::endl;
                if (it != var_map.end())
                {
                    //std::cout << "Constant from XML:" << tool.first_attribute().as_string();
                    it->second >> tool.first_attribute().value();
                    //std::cout << "Constant from XML:" << it->second <<  std::endl;
                }
            }
        }
        else
        {
            char the_current_path[255];
            if (!getcwd(the_current_path, sizeof(the_current_path)))
            {
                the_current_path[0] = '\\';
            }

            the_current_path[sizeof(the_current_path) - 1] = '\0'; /* not really required */

            d_exception ex(result, the_current_path);
            throw ex;
            //std::cerr << "xml not loaded, reason: " << result.status << std::endl;
            //return false;
        }
        return true;
    }

private:
    bool xml_data;
};

/**
 * \struct variables
 * \brief should get and store mutable input data
 *
 */
struct variables : public var_base
{
    variables() :
        xml_data(false)
    {
        var_map["B"] = cnst::Bk;
        ///following values will be transmitted from input QSTRING
        //var_map["L"] = cnst::Lk;
        //var_map["T"] = cnst::T;
        //var_map["D"] = cnst::D;
        //var_map["V"] = cnst::V;
        //var_map["P"] = cnst::V;

        var_map["N_1"];
        var_map["N_2"];
        var_map["Z"];
        var_map["M_koef"];

        var_map["Lambda_1"];
        var_map["Lambda_2"];
        var_map["Lambda_3"];
        var_map["Lambda_4"];
        var_map["Lambda_5"];
        var_map["Fr_map"];

        var_map["Lambda_35"];
        var_map["Lambda_375"];
        var_map["Lambda_40"];
        var_map["Lambda_425"];
        var_map["Lambda_45"];
        var_map["Lambda_475"];
        var_map["Lambda_50"];
        var_map["Lambda_525"];
        var_map["Lambda_55"];

        var_map["H_1"];
        var_map["H_2"];
        var_map["Lambda"];

        var_map["L_1"];
        var_map["N_22"];
        var_map["G_1"];
        var_map["G_2"];

        var_map["Sigma_35"];
        var_map["Sigma_36"];
        var_map["Sigma_37"];
        var_map["Sigma_38"];
        var_map["Sigma_39"];
        var_map["Sigma_40"];
        var_map["Sigma_41"];
        var_map["Sigma_42"];
        var_map["Sigma_43"];
        var_map["Sigma_44"];
        var_map["Sigma_45"];
        var_map["Sigma_46"];
        var_map["Sigma_47"];
        var_map["Sigma_48"];
        var_map["Sigma_49"];
        var_map["Sigma_50"];
        var_map["Sigma_51"];
        var_map["Sigma_52"];

        var_map["Fr_map2"];
        var_map["sigma_map2"];

        var_map["F_1"];
        var_map["F_2"];
    }

    variables(const variables& var_copy)
    {
        var_map = var_copy.var_map;
    }

    size_t size()
    {
        return var_map.size();
    }

    variables& operator= (const variables& var_copy)
    {
        var_map = var_copy.var_map;
        return *this;
    }

private:
    bool xml_data;
};



#endif // CONSTANTS_HOLDER_HPP
