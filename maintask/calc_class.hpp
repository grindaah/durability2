#ifndef CALC_CLASS_HPP
#  define CALC_CLASS_HPP

#  include "constants_holder.hpp"
#  include "interpolate.hpp"
#  include "ribs_connections.hpp"
#  include "input_interface.hpp"
#  include "task_results.hpp"
#  include "logger.hpp"

#  include <vector>
#  include <QString>
/**
 * \class calc_class
 * \brief main calculation class for durability
 *
 */
class calc_class
{
    ///not copy constructable
    calc_class(const calc_class&);
    calc_class& operator=(const calc_class&);

    bool choose_lambda(const double sigma_0
                     , std::string* str1
                     , std::string* str2
                     , double& diff
                     );
    bool choose_Fr(const double Fr
                   , std::string* str1
                   , std::string* str2
                   , double& diff
                   );
    variables vars;
    variables vars_copy;                                   // for diagram
    ribs_holder rh_copy;                                   // for diagram
    task_results result;
    std::vector<rib_frame_inputs> rib_results;

    logger log;

    void get_impulse_rib_results(std::vector<double> M_h1_vv_vector
                               , std::vector<double> M_h1_pv_vector
                               , std::vector<double> M_h2_vv_vector
                               , std::vector<double> M_h2_pv_vector
                               , std::vector<double> M_vd_vector
                               , std::vector<double> M_pd_vector
                               , std::vector<double> Q_vv_vector
                               , std::vector<double> Q_pv_vector
                               );

    void get_dynamic_rib_results(std::vector<double> Q_vd_vector
                               , std::vector<double> Q_pd_vector
                               );

    void get_damaged_external_forces(
                                 const std::vector<double>& M_h2_vv_vector
                               , const std::vector<double>& M_h2_pv_vector
                               , const std::vector<double>& M_vd_vector
                               , const std::vector<double>& M_pd_vector
        );

    void get_sigmas();

public:
    calc_class():
        rib_results(3)
      , log(log_debug)
    {
        std::cerr << "creating calc class" << std::endl;

        ///temp data for check sequence of steps:
        rib_results.reserve(3);
    }

    ///data members
    input_interface inputs;

    ///methods
    void prepare(const char* file_name);
    void prepare(const int);

    QString calculate(const double V, const int step);
    void calc_damaged(const double V, const double m_koef);
    QString calc_ribs_connections();
    void calc_ribs_connections(bool diag);
    void get_nzp_min(double n_m_min, double n_q_min);
    void get_nzp_combat_min(double n_combat_min);
    bool has_data();
    QString get_result();
    QString get_diag_result(const int V, const int step);
};

#endif // CALC_CLASS_HPP
