#include "calc_class.hpp"
#include "interpolate.hpp"
#include "compare.hpp"
#include <exception>
#include <set>


bool calc_class::has_data()
{
    return true;
}

QString calc_class::get_result()
{
    return result.give_results();
}

QString calc_class::get_diag_result(const int V, const int step)
{
    return result.give_diag_results(V, step);
}

bool calc_class::choose_Fr(const double sigma
                        , std::string *str1
                        , std::string *str2
                        , double &diff
                        )
{
    if (cmpr::greater(sigma, 5.2))
    {
        std::cerr << "cannot interpolate kd, sigma value is more than 5.2!" << std::endl;
        return false;
    }
    ///\note When T - is too big sigma values can be very low
    ///      to continue calc we consider them as minimal 3.5
    if (cmpr::less(sigma, 3.5))
    {
        std::cerr << "warning: sigma is less then 3.5";
        *str1 = "Sigma_35";
        *str2 = "Sigma_36";
        diff = 0.;
        return true;
    }
    double border1, border2;
    for (int i = 0; i < 18; ++i)
    {
        border1 = 3.5 + i * 0.1;
        border2 = 3.5 + (i+1) * 0.1;
        if (cmpr::greater(sigma, border1) && cmpr::less(sigma, border2))
        {
            std::stringstream ss;
            ss << border1*10;

            *str2 = "Sigma_" + ss.str();
            ss.seekp(0);
            ss << border2*10;
            *str1 = "Sigma_" + ss.str();
            diff = sigma - border1;
            return true;
        }
    }
    return false;
}

bool calc_class::choose_lambda(const double sigma_0
                             , std::string* str1
                             , std::string* str2
                             , double& diff
                             )
{
    if (sigma_0 > 5.5) // || sigma_0 < 3.5)
    {
        std::cerr << "cannot interpolate lambda_dash_1, sigma_0 not inside interval!" << std::endl;
        return false;
    }

    if (sigma_0 < 3.5)
    {
        *str2 = "Lambda_35";
        *str1 = "Lambda_375";
        diff = 0.;
    }

    if (cmpr::greater(sigma_0, 3.5) && cmpr::less(sigma_0, 3.75))
    {
        *str2 = "Lambda_35";
        *str1 = "Lambda_375";
        diff = sigma_0 - 3.5;
    }
    else if (cmpr::greater(sigma_0, 3.75) && cmpr::less(sigma_0, 4.0))
    {
        *str2 = "Lambda_375";
        *str1 = "Lambda_40";
        diff = sigma_0 - 3.75;
    }
    else if (cmpr::greater(sigma_0, 4.0) && cmpr::less(sigma_0, 4.25))
    {
        *str2 = "Lambda_40";
        *str1 = "Lambda_425";
        diff = sigma_0 - 4.;
    }
    else if (cmpr::greater(sigma_0, 4.25) && cmpr::less(sigma_0, 4.5))
    {
        *str2 = "Lambda_425";
        *str1 = "Lambda_45";
        diff = sigma_0 - 4.25;
    }
    else if (cmpr::greater(sigma_0, 4.5) && cmpr::less(sigma_0, 4.75))
    {
        *str2 = "Lambda_45";
        *str1 = "Lambda_475";
        diff = sigma_0 - 4.5;
    }
    else if (cmpr::greater(sigma_0, 4.75) && cmpr::less(sigma_0, 5.))
    {
        *str2 = "Lambda_475";
        *str1 = "Lambda_50";
        diff = sigma_0 - 4.75;
    }
    else if (cmpr::greater(sigma_0, 5.) && cmpr::less(sigma_0, 5.25))
    {
        *str2 = "Lambda_50";
        *str1 = "Lambda_525";
        diff = sigma_0 - 5.;
    }
    else if (cmpr::greater(sigma_0, 5.25) && cmpr::less(sigma_0, 5.5))
    {
        *str2 = "Lambda_525";
        *str1 = "Lambda_55";
        diff = sigma_0 - 5.25;
    }
    else if (cmpr::equal(sigma_0, 3.5))
    {
        *str1 = "Lambda_35";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0, 3.75))
    {
        *str1 = "Lambda_375";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0, 4.0))
    {
        *str1 = "Lambda_40";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0, 4.25))
    {
        *str1 = "Lambda_425";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0, 4.5))
    {
        *str1 = "Lambda_45";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0, 4.75))
    {
        *str1 = "Lambda_475";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0, 5.))
    {
        *str1 = "Lambda_50";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0, 5.25))
    {
        *str1 = "Lambda_525";
        diff = 0.;
    }
    else if (cmpr::equal(sigma_0,sigma_0, 5.5))
    {
        *str1 = "Lambda_55";
        diff = 0.;
    }
    return true;
}

QString calc_class::calculate(const double V, const int step)
{
    double m_koef = 1.0;

    if (cmpr::greater_or_equal(V, 0.))
        vars.var_map["V"] = V;
    if (step > -1)
        m_koef = vars.var_map["M_koef"][step];

    log << "L = " << vars.var_map["L"].get()
              << ", V = " << vars.var_map["V"].get();
    try
    {
        double Fr_t = vars.var_map["V"].get() * 0.514 /
                  sqrt(vars.var_map["L"].get() * cnst::g);
        log << "Fr_t = " << Fr_t ;

        cubic_spline spl;
        assert (vars.var_map["N_1"].size() == vars.var_map["Z"].size() && "mismatch in size N_1 and Z!");
        std::vector<double> x_vec(vars.var_map["Z"].get_vector());
        std::vector<double> y_vec(vars.var_map["N_1"].get_vector());
        double* x = &x_vec[0];
        double* y = &y_vec[0];

        spl.build_spline(x, y, x_vec.size());
        log << "spline ready" ;
        double nv_1 = spl.f(vars.var_map["L"]);

        std::vector<double> y_vec2(vars.var_map["N_2"].get_vector());
        y = &y_vec2[0];
        spl.build_spline(x, y, x_vec.size());
        double nv_2 = spl.f(vars.var_map["L"]);
        log << "nv_1 = " << nv_1 ;
        log << "nv_2 = " << nv_2 ;

        double Fr_v1 = nv_1 * Fr_t;
        double Fr_v2 = nv_2 * Fr_t;
        log << "Fr_v1 = " << Fr_v1 << ", Fr_v2 = " << Fr_v2 ;

        double sigma_0 = 0.836 * sqrt(vars.var_map["L"] /
                                      vars.var_map["T"]);

        log << "sigma_0 = " << sigma_0 ;

        ///Блок определение длины расчетной волны
        //далее по sigma_0 выбираем два ближайших графика Lambda
        std::string str1,str2;
        double diff;
        if (!choose_lambda(sigma_0, &str1, &str2, diff))
            std::cerr << "Unable to choose Lambda from interval";
        log << "intervals chosen: " << str1
                  << " "<< str2 ;

        x_vec.assign(vars.var_map["Fr_map"].get_vector().begin()
                   , vars.var_map["Fr_map"].get_vector().end()
                   );
        y_vec.assign(vars.var_map[str1].get_vector().begin()
                   , vars.var_map[str1].get_vector().end()
                   );
        x = &x_vec[0];
        y = &y_vec[0];
        /// Расчет lamda по нижнему графику
        spl.build_spline(x, y, x_vec.size());
        double lambda_lower = spl.f(Fr_v1);
        double lambda_lower2 = spl.f(Fr_v2);

        /// Расчет lambda по верхнему графику
        cubic_spline spl2;
        y_vec2.assign(vars.var_map[str2].get_vector().begin()
                   , vars.var_map[str2].get_vector().end()
                   );

        y = &y_vec2[0];
        spl2.build_spline(x, y, x_vec.size());
        double lambda_upper = spl2.f(Fr_v1);
        double lambda_upper2 = spl2.f(Fr_v2);

        log << "lambda_lower = " << lambda_lower
                  << ", lambda_upper = " << lambda_upper
                  ;

        //double lambda_dash_1 = (lambda_lower + lambda_upper) / 2.;
        double lambda_dash_1 = lambda_upper -
                (lambda_upper - lambda_lower) * (diff/0.25);
        double lambda_dash_2 = lambda_upper2 -
                (lambda_upper2 - lambda_lower2) * (diff/0.25);

        double lambda_1 = lambda_dash_1 * vars.var_map["L"];
        double lambda_2 = lambda_dash_2 * vars.var_map["L"];

        log << "lambda_dash_ 1 = " << lambda_dash_1 ;
        log << "lambda_1 = " << lambda_1 ;
        log << "lambda_dash_ 2 = " << lambda_dash_2 ;
        log << "lambda_2 = " << lambda_2 ;

        //Определение высоты расчетной волны
        x_vec.assign(vars.var_map["Lambda"].get_vector().begin()
                   , vars.var_map["Lambda"].get_vector().end()
                   );
        y_vec.assign(vars.var_map["H_1"].get_vector().begin()
                   , vars.var_map["H_1"].get_vector().end()
                   );
        y_vec2.assign(vars.var_map["H_2"].get_vector().begin()
                    , vars.var_map["H_2"].get_vector().end()
                    );

        x = &x_vec[0];
        y = &y_vec[0];
        spl2.build_spline(x, y, x_vec.size());
        double h_1 = spl2.f(lambda_1);                      // lambda_1 -  длина расчетной волны

        y = &y_vec2[0];
        spl2.build_spline(x, y, x_vec.size());
        double h_2 = spl2.f(lambda_2);

        log << "h_1 = " << h_1
                  << "h_2 = " << h_2 ;
        // Определение коэффициентов, входящих в формулу волнового момента
        ///\todo check if i should get alpha and delta from external src?
        //double k_pv = 1.3 * vars.var_map["alpha"].get()
        //         * (0.032 + 0.013(vars.var_map["delta"]) )
        log << "k_pv = " << cnst::k_pv ;
        log << "k_vv = " << cnst::k_vv ;

        double k_11 = 1 - (3 + 25.* Fr_v1) * (cnst::M_t
                 / (vars.var_map["D"] * vars.var_map["L"]))
                 + 1.5 * Fr_v1;
        double k_12 = 1 - (3 + 25.* Fr_v2) * (cnst::M_t
                  / (vars.var_map["D"] * vars.var_map["L"]))
                 + 1.5 * Fr_v2;
        double k_2 = -2. *  vars.var_map["T"] / vars.var_map["L"]
                    - 1.25 *  vars.var_map["B"] / vars.var_map["L"] + 0.835;

        log << "k_11 = " << k_11 ;
        log << "k_12 = " << k_12 ;
        log << "k_2 = "  << k_2  ;

        // Определение значения величины  изгибающих волновых
        // (ВИМ) моментов на миделе (по середине) (максимальные)
        double local_L = vars.var_map["L"];
        double local_B = vars.var_map["B"];
        log << "k_2=" <<  k_2;
        double M_h1_vv = m_koef * h_1 / 2. * cnst::k_vv * k_11 * k_2
                * local_B * local_L * local_L;
        double M_h1_pv = m_koef * -h_1 *cnst::k_pv * k_11 * k_2
                * local_B *local_L * local_L / 2.;
        double M_h2_vv = m_koef * h_2 * cnst::k_vv * k_12 * k_2
                * vars.var_map["B"].get() * vars.var_map["L"].get() * vars.var_map["L"].get() / 2.;
        double M_h2_pv = m_koef * -h_2 * cnst::k_pv * k_12 * k_2
                * vars.var_map["B"].get() * vars.var_map["L"].get() * vars.var_map["L"].get() / 2.;

        log << "M_h1_vv = " << M_h1_vv
                  << "M_h1_pv = " << M_h1_pv
                  << "M_h2_vv = " << M_h2_vv
                  << "M_h2_pv = " << M_h2_pv ;

        // Определение значения величины волновых составляющих перерезывающих сил (ПС) на четверти длины корабля (максимальные)
        double Q_h1_vv = 3.5 * M_h1_vv / vars.var_map["L"].get();
        double Q_h1_pv = 3.5 * M_h1_pv / vars.var_map["L"].get();
        double Q_h2_vv = 3.5 * M_h2_vv / vars.var_map["L"].get();
        double Q_h2_pv = 3.5 * M_h2_pv / vars.var_map["L"].get();

        log << "Q_h1_vv = " << Q_h1_vv
                  << "Q_h1_pv = " << Q_h1_pv
                  << "Q_h2_vv = " << Q_h2_vv
                  << "Q_h2_pv = " << Q_h2_pv ;

        //Распределение волновых изгибающих  моментов и
        // волновых составляющих перерезывающихсил по длине корабля
        constants_holder<double> M_h1_vv_func
                       , M_h1_pv_func
                       , M_h2_vv_func
                       , M_h2_pv_func;
        M_h1_vv_func.setname("M_h1_vv_func");
        M_h1_pv_func.setname("M_h1_pv_func");
        M_h2_vv_func.setname("M_h2_vv_func");
        M_h2_pv_func.setname("M_h2_pv_func");

        std::vector<double> M_l(vars.var_map["L_1"].get_vector());
        std::vector<double> N_2(vars.var_map["N_22"].get_vector());

        cubic_spline* p_spline_M_l = new cubic_spline();
        x_vec.assign(N_2.begin(), N_2.end());
        y_vec.assign(M_l.begin(), M_l.end());
        x = &x_vec[0];
        y = &y_vec[0];
        p_spline_M_l->build_spline(x, y, x_vec.size());
        for (size_t i = 0; i < 21; ++i)
        {
            M_h1_vv_func.append (p_spline_M_l->f(i) * M_h1_vv);
            M_h1_pv_func.append (p_spline_M_l->f(i) * M_h1_pv);
            M_h2_vv_func.append (p_spline_M_l->f(i) * M_h2_vv);
            M_h2_pv_func.append (p_spline_M_l->f(i) * M_h2_pv);
            //M_h2_max_vv_func.append (p_spline_M_l->f(i)* M_max_h2_vv);
            //M_h2_max_pv_func.append (p_spline_M_l->f(i)* M_max_h2_pv);
        }
        delete p_spline_M_l;

        log << "M_h1_vv_func = " << M_h1_vv_func
                  << "M_h1_pv_func = " << M_h1_pv_func
                  << "M_h2_vv_func = " << M_h2_vv_func
                  << "M_h2_pv_func = " << M_h2_pv_func ;

        constants_holder<double> Q_h1_vv_func
                       , Q_h1_pv_func
                       , Q_h2_vv_func
                       , Q_h2_pv_func;
        Q_h1_vv_func.setname("Q_h1_vv_func");
        Q_h1_pv_func.setname("Q_h1_pv_func");
        Q_h2_vv_func.setname("Q_h2_vv_func");
        Q_h2_pv_func.setname("Q_h2_pv_func");

        std::vector<double> Q_l(vars.var_map["G_1"].get_vector());
        x_vec.assign(N_2.begin(), N_2.end());
        y_vec.assign(Q_l.begin(), Q_l.end());
        x = &x_vec[0];
        y = &y_vec[0];
        cubic_spline* p_spline_Q_l = new cubic_spline();
        p_spline_Q_l->build_spline(x, y, y_vec.size());
        for (size_t i = 0; i < 21; ++i)
        {
            Q_h1_vv_func.append (p_spline_Q_l->f(i) * Q_h1_vv);
            Q_h1_pv_func.append (p_spline_Q_l->f(i) * Q_h1_pv);
            Q_h2_vv_func.append (p_spline_Q_l->f(i) * Q_h2_vv);
            Q_h2_pv_func.append (p_spline_Q_l->f(i) * Q_h2_pv);
        }
        delete p_spline_Q_l;

        log << "Q_h1_vv_func = " << Q_h1_vv_func
                  << "Q_h1_pv_func = " << Q_h1_pv_func
                  << "Q_h2_vv_func = " << Q_h2_vv_func
                  << "Q_h2_pv_func = " << Q_h2_pv_func ;

        //Определение значений величины динамического изгибающего момента
        // и динамической составляющей перерезывающей силы на миделе
        diff = 0.;
        str1.clear();
        str2.clear();
        if (!choose_Fr(sigma_0, &str1, &str2, diff))
        {
            std::cerr << "Unable to choose Fr from interval";
            d_exception ex(d_exception::interpolate_error_Fr);
            throw ex;
        }

        log << "intervals chosen: " << str1
                  << " "<< str2 ;

        x_vec.assign(vars.var_map["sigma_map2"].get_vector().begin()
                   , vars.var_map["sigma_map2"].get_vector().end()
                   );
        y_vec.assign(vars.var_map[str1].get_vector().begin()
                   , vars.var_map[str1].get_vector().end()
                   );
        x = &x_vec[0];
        y = &y_vec[0];

        log << vars.var_map["sigma_map2"]
                  << vars.var_map[str1]
                  << vars.var_map[str2]      ;

        cubic_spline* pspline = new cubic_spline();
        pspline->build_spline(x,y, x_vec.size());
        double k_d_upper = pspline->f(Fr_v2);
        delete pspline;

        y_vec2.assign(vars.var_map[str2].get_vector().begin()
                   , vars.var_map[str2].get_vector().end()
                   );

        y = &y_vec2[0];
        cubic_spline* psp_line2 = new cubic_spline();
        psp_line2->build_spline(x, y, x_vec.size());
        double k_d_lower = psp_line2->f(Fr_v2);
        delete psp_line2;

        double k_d = k_d_lower + (k_d_upper - k_d_lower)*(diff/0.1);
        log << "k_d_lower: " << k_d_lower
                  << "k_d_upper: " << k_d_upper
                  << "k_d: " << k_d ;

        //Максимальные значения ДИМ на миделе:
        //Динамический изгибающий момент на подошве волны для РБИ (и только)
        double M_dp = -h_2 * k_d * local_B*local_B * local_L / 2.;
        //ДИМ на вершине волны для РБИ
        double M_dv = -0.4 * M_dp;
        log << "M_dp: " << M_dp
                  << "M_dv: " << M_dv ;
        //Значения динамич ПС на четверти длины корабля (максимальные)
        double Q_h2_vv2 = 3.5 * M_dv / local_L;
        double Q_h2_pv2 = 3.5 * M_dp / local_L;
        log << "Q_h2_vv2: " << Q_h2_vv2
                  << "Q_h2_pv2: " << Q_h2_pv2 ;
        //M_dp function here

        //////////////
        constants_holder<double>
                         M_dp_func
                       , M_dv_func;
        M_dp_func.setname("M_dp_func");
        M_dv_func.setname("M_dv_func");

        std::vector<double> F_1(vars.var_map["F_1"].get_vector());
        std::vector<double> F_2(vars.var_map["F_2"].get_vector());
        log << vars.var_map["F_1"];
        log << vars.var_map["F_2"];

        cubic_spline* p_spline_M_dp = new cubic_spline();
        x_vec.assign(F_2.begin(), F_2.end());
        y_vec.assign(F_1.begin(), F_1.end());
        x = &x_vec[0];
        y = &y_vec[0];
        p_spline_M_dp->build_spline(x, y, x_vec.size());
        for (int i = 0; i < 21; ++i)
        {
            M_dp_func.append (p_spline_M_dp->f(i)* M_dp);
            M_dv_func.append (p_spline_M_dp->f(i)* M_dv);
        }

        delete p_spline_M_dp;
        log << "end of 1st part\n";
        log << "M_dp_func = " << M_dp_func
                  << "M_dv_func = " << M_dv_func ;

        ///getting the resulting forces for rib_frames:
        get_impulse_rib_results(M_h1_vv_func.get_vector()
                              , M_h1_pv_func.get_vector()
                              , M_h2_vv_func.get_vector()
                              , M_h2_pv_func.get_vector()
                              , M_dv_func.get_vector()
                              , M_dp_func.get_vector()
                              , Q_h2_vv_func.get_vector()
                              , Q_h2_pv_func.get_vector()
                              );
        calc_damaged(vars.var_map["V"], m_koef);
        get_sigmas();

        QString warnings;

        if ((-1 == step) ||
            (cmpr::equal(V, 2.) && step == 0))
        ///if regular calc or first step of diagram calculation
            warnings = calc_ribs_connections();
        else
        ///else calling function which reuse stored state for calc
            calc_ribs_connections(true);

        return warnings;
    }
    catch(std::exception& e)
    {
        throw d_exception(e);
    }

    return QString();
}

void calc_class::calc_damaged(const double V, const double m_koef)
{
    prepare("tree2.xml");
    log << "========================================"
              ;

    if (cmpr::greater_or_equal(V, 0.))
    {
        vars.var_map["V"] = V;
    }

    log << "L = " << vars.var_map["L"].get()
              << ", V = " << vars.var_map["V"].get() ;

    ///data for third part of calculations
    try
    {
        double Fr_t = vars.var_map["V"].get() * 0.514 /
                  sqrt(vars.var_map["L"].get() * cnst::g);
        log << "Fr_t = " << Fr_t ;

        std::vector<double> x_vec(vars.var_map["Z"].get_vector());
        std::vector<double> y_vec(vars.var_map["N_2"].get_vector());
        double* x = &x_vec[0];
        double* y = &y_vec[0];

        cubic_spline* pspline = new cubic_spline();

        pspline->build_spline(x, y, x_vec.size());
        double nv_2 = pspline->f(vars.var_map["L"]);
        log << "nv_2 = " << nv_2 ;
        delete pspline;

        double Fr_v2 = nv_2 * Fr_t;
        log << "Fr_v2: " << Fr_v2 ;

        double sigma_0 = 0.836 * sqrt(vars.var_map["L"] /
                                      vars.var_map["T"]);

        log << "sigma_0 = " << sigma_0 ;

        std::string str1,str2;
        double diff;
        if (!choose_lambda(sigma_0, &str1, &str2, diff))
            std::cerr << "Unable to choose Lambda from interval";
        log << "intervals chosen: " << str1
                  << " "<< str2 ;

        x_vec.assign(vars.var_map["Fr_map"].get_vector().begin()
                   , vars.var_map["Fr_map"].get_vector().end()
                   );
        y_vec.assign(vars.var_map[str1].get_vector().begin()
                   , vars.var_map[str1].get_vector().end()
                   );
        x = &x_vec[0];
        y = &y_vec[0];
        /// Расчет lamda по нижнему графику
        cubic_spline spl;
        spl.build_spline(x, y, x_vec.size());
        double lambda_lower2 = spl.f(Fr_v2);

        /// Расчет lambda по верхнему графику
        cubic_spline spl2;
        y_vec.assign(vars.var_map[str2].get_vector().begin()
                   , vars.var_map[str2].get_vector().end()
                   );

        y = &y_vec[0];
        spl2.build_spline(x, y, x_vec.size());
        double lambda_upper2 = spl2.f(Fr_v2);

        log << "lambda_lower2 = " << lambda_lower2
                  << ", lambda_upper2 = " << lambda_upper2
                  ;

        double lambda_dash_2 = lambda_upper2 -
                (lambda_upper2 - lambda_lower2) * (diff/0.25);
        double lambda_2 = lambda_dash_2 * vars.var_map["L"];

        log << "lambda_dash_2 = " << lambda_dash_2 ;
        log << "lambda_2 = " << lambda_2 ;

        //Определение высоты расчетной волны
        x_vec.assign(vars.var_map["Lambda"].get_vector().begin()
                   , vars.var_map["Lambda"].get_vector().end()
                   );
        y_vec.assign(vars.var_map["H_2"].get_vector().begin()
                    , vars.var_map["H_2"].get_vector().end()
                    );

        x = &x_vec[0];
        y = &y_vec[0];
        spl2.build_spline(x, y, x_vec.size());
        double h_2 = spl2.f(lambda_2);                      // lambda_1 -  длина расчетной волны

        log << "h_2 = " << h_2 ;

        // Определение коэффициентов, входящих в формулу волнового момента
        ///\todo check if i should get alpha and delta from external src?
        //double k_pv = 1.3 * vars.var_map["alpha"].get()
        //         * (0.032 + 0.013(vars.var_map["delta"]) )
        log << "k_pv = " << cnst::k_pv
                  << "k_vv = " << cnst::k_vv ;

        log << "D = " << vars.var_map["D"]
                  << "P = " << vars.var_map["L"]
                  << "B = " << vars.var_map["B"] ;

        double k_1 = 1 - ((3 + 25.* Fr_v2)
                         * (cnst::M_t
                 / ((vars.var_map["D"] + vars.var_map["P"] )* vars.var_map["L"])
                            )
                  ) + 1.5 * Fr_v2;

        double k_2 = -2. *  vars.var_map["T"] / vars.var_map["L"]
                    - 1.25 *  vars.var_map["B"] / vars.var_map["L"] + 0.835;

        log << "k_1 = " << k_1 ;
        log << "k_2 = " << k_2 ;

        /// ?? Максимальные изгибающие моменты?
        double local_L = vars.var_map["L"];
        double local_B = vars.var_map["B"];
        log << "L :" << local_L << ", B: " << local_B;
        double M_max_h2_vv = m_koef * h_2 * cnst::k_vv * k_1 * k_2
                * local_B * local_L * local_L / 2.;
        double M_max_h2_pv = m_koef * -h_2 * cnst::k_pv * k_1 * k_2
                * local_B * local_L * local_L / 2.;

        log << "M_max_h2_vv = " << M_max_h2_vv
                  << "M_max_h2_pv = " << M_max_h2_pv ;

        ///////////////////////////////////////////////////
        // Определение значения величины волновых составляющих перерезывающих сил (ПС) на четверти длины корабля (максимальные)

        //Распределение волновых изгибающих  моментов и
        // волновых составляющих перерезывающихсил по длине корабля
        constants_holder<double>
                         M_h2_max_vv_func
                       , M_h2_max_pv_func;
        M_h2_max_vv_func.setname("M_h2_max_vv_func");
        M_h2_max_pv_func.setname("M_h2_max_pv_func");

        std::vector<double> M_l(vars.var_map["L_1"].get_vector());
        std::vector<double> N_2(vars.var_map["N_22"].get_vector());

        cubic_spline* p_spline_M_l = new cubic_spline();
        x_vec.assign(N_2.begin(), N_2.end());
        y_vec.assign(M_l.begin(), M_l.end());
        x = &x_vec[0];
        y = &y_vec[0];
        p_spline_M_l->build_spline(x, y, x_vec.size());
        for (int i = 0; i < 21; ++i)
        {
            M_h2_max_vv_func.append (p_spline_M_l->f(i)* M_max_h2_vv);
            M_h2_max_pv_func.append (p_spline_M_l->f(i)* M_max_h2_pv);
        }
        delete p_spline_M_l;
        log << "M_h2_max_vv_func = " << M_h2_max_vv_func
                  << "M_h2_max_pv_func = " << M_h2_max_pv_func ;

        double Q_h2_vv = 3.5 * M_max_h2_vv / local_L;
        double Q_h2_pv = 3.5 * M_max_h2_pv / local_L;
        log << "Q_h2_vv2: " << Q_h2_vv
                  << "Q_h2_pv2: " << Q_h2_pv ;

        constants_holder<double>
                         Q_h2_vv_func
                       , Q_h2_pv_func;
        Q_h2_vv_func.setname("Q_h2_vv_func");
        Q_h2_pv_func.setname("Q_h2_pv_func");

        std::vector<double> G_1(vars.var_map["G_1"].get_vector());
        std::vector<double> G_2(vars.var_map["G_2"].get_vector());

        cubic_spline* p_spline_Q_l = new cubic_spline();

        x = &G_2[0];
        y = &G_1[0];
        p_spline_Q_l->build_spline(x, y, x_vec.size());
        for (int i = 0; i < 21; ++i)
        {
            Q_h2_vv_func.append (p_spline_Q_l->f(i)* Q_h2_vv);
            Q_h2_pv_func.append (p_spline_Q_l->f(i)* Q_h2_pv);
        }
        /// getting results for theoretical frames
        ///\todo function \n imbue here
        /// vd - dynamic moment
        delete p_spline_Q_l;
        log << "Q_h2_vv_func = " << Q_h2_vv_func
                  << "Q_h2_pv_func = " << Q_h2_pv_func ;

        ///////////
        //Определение значений величины динамического изгибающего момента
        // и динамической составляющей перерезывающей силы на миделе
        diff = 0.;
        str1.clear();
        str2.clear();
        if (!choose_Fr(sigma_0, &str1, &str2, diff))
        {
            std::cerr << "Unable to choose Fr from interval";
            d_exception ex(d_exception::interpolate_error_Fr);
            throw ex;
        }
        log << "intervals chosen: " << str1
                  << " "<< str2 ;

        x_vec.assign(vars.var_map["sigma_map2"].get_vector().begin()
                   , vars.var_map["sigma_map2"].get_vector().end()
                   );
        y_vec.assign(vars.var_map[str1].get_vector().begin()
                   , vars.var_map[str1].get_vector().end()
                   );

        log << vars.var_map["sigma_map2"]
                  << vars.var_map[str1]
                  << vars.var_map[str2]      ;
        x = &x_vec[0];
        y = &y_vec[0];

        cubic_spline* pspline_kd1 = new cubic_spline();
        pspline_kd1->build_spline(x,y, x_vec.size());
        double k_d_lower = pspline_kd1->f(Fr_v2);
        delete pspline_kd1;

        std::vector<double> y_vec2(vars.var_map[str2].get_vector());

        y = &y_vec2[0];
        cubic_spline* psp_line_kd2 = new cubic_spline();
        psp_line_kd2->build_spline(x, y, x_vec.size());
        double k_d_upper = psp_line_kd2->f(Fr_v2);
        delete psp_line_kd2;

        double k_d = k_d_lower + (k_d_upper - k_d_lower)*(diff/0.1);
        log << "k_d_lower: " << k_d_lower
                  << "k_d_upper: " << k_d_upper
                  << "k_d: " << k_d ;

        //Максимальные значения ДИМ на миделе:
        //Динамический изгибающий момент на подошве волны для РБИ (и только)
        double M_dp = -h_2 * k_d * local_B*local_B * local_L / 2.;
        //ДИМ на вершине волны для РБИ
        double M_dv = -0.4 * M_dp;
        log << "M_dp: " << M_dp
                  << "M_dv: " << M_dv ;

        Q_h2_vv = 3.5 * M_dv / local_L;
        Q_h2_pv = 3.5 * M_dp / local_L;

        //////////////
        constants_holder<double>
                         M_dp_func
                       , M_dv_func;
        M_dp_func.setname("M_dp_func");
        M_dv_func.setname("M_dv_func");

        std::vector<double> F_1(vars.var_map["F_1"].get_vector());
        std::vector<double> F_2(vars.var_map["F_2"].get_vector());
        log << vars.var_map["F_1"];
        log << vars.var_map["F_2"];

        x_vec.assign(F_2.begin(), F_2.end());
        y_vec.assign(F_1.begin(), F_1.end());
        y_vec2.assign(F_1.begin(), F_1.end());
        for (size_t i=0; i < y_vec.size(); ++i)
        {
            y_vec[i] = y_vec[i] * M_dp;
            y_vec2[i] = y_vec2[i] * M_dv;
        }
        x = &x_vec[0];
        y = &y_vec[0];

        cubic_spline* p_spline_M_dp = new cubic_spline();
        p_spline_M_dp->build_spline(x, y, x_vec.size());

        for (int i = 0; i < 21; ++i)
            M_dp_func.append (p_spline_M_dp->f(i));

        delete p_spline_M_dp;

        cubic_spline* p_spline_M_dv = new cubic_spline();
        y = &y_vec2[0];
        p_spline_M_dv->build_spline(x, y, x_vec.size());
        for (int i = 0; i < 21; ++i)
            M_dv_func.append (p_spline_M_dv->f(i));

        delete p_spline_M_dv;

        log << "M_dp_func = " << M_dp_func
                  << "M_dv_func = " << M_dv_func ;

        std::vector<double> M_dp_func_res = M_dp_func.get_vector();

        get_dynamic_rib_results(
                               Q_h2_vv_func.get_vector()
                              , Q_h2_pv_func.get_vector()
                              );

        get_damaged_external_forces(M_h2_max_vv_func.get_vector()
                                  , M_h2_max_pv_func.get_vector()
                                  , M_dv_func.get_vector()
                                  , M_dp_func.get_vector()
                                  );
        result.M_sum_max.assign(M_dv_func.get_vector().begin(), M_dv_func.get_vector().end());
        result.Q_sum_max.assign(Q_h2_pv_func.get_vector().begin(), Q_h2_pv_func.get_vector().end());
///\note second time the same? should i move this block to the end?
        ///Q(l):
//        cubic_spline* p_spline_Q_l = new cubic_spline();

//        x = &G_2[0];
//        y = &G_1[0];
//        p_spline_Q_l->build_spline(x, y, x_vec.size());
//        for (int i = 0; i < 21; ++i)
//        {
//            Q_h2_vv_func.append (p_spline_Q_l->f(i)* Q_h2_vv);
//            Q_h2_pv_func.append (p_spline_Q_l->f(i)* Q_h2_pv);
//        }
//        delete p_spline_Q_l;
//        log << "Q_h2_vv_func = " << Q_h2_vv_func
//                  << "Q_h2_pv_func = " << Q_h2_pv_func ;


    }
    catch(std::exception& e)
    {
        throw d_exception(e);
    }
}

void calc_class::get_impulse_rib_results(
                                    std::vector<double> M_h1_vv_vector
                                  , std::vector<double> M_h1_pv_vector
                                  , std::vector<double> M_h2_vv_vector
                                  , std::vector<double> M_h2_pv_vector
                                  , std::vector<double> M_vd_vector
                                  , std::vector<double> M_pd_vector
                                  , std::vector<double> Q_vv_vector
                                  , std::vector<double> Q_pv_vector
                                  )
{
    ///\todo mapping of ribframe numbers!!!
    rib_results[0].M_h1_vv = M_h1_vv_vector[4];
    rib_results[0].M_h1_pv = M_h1_pv_vector[4];
    rib_results[0].M_h2_vv = M_h2_vv_vector[4];
    rib_results[0].M_h2_pv = M_h2_pv_vector[4];
    rib_results[0].M_vd = M_vd_vector[4];
    rib_results[0].M_pd = M_pd_vector[4];
    rib_results[0].Q_vv = Q_vv_vector[4];
    rib_results[0].Q_pv = Q_pv_vector[4];
    rib_results[0].koef_n = 0.96;

    rib_results[1].M_h1_vv = M_h1_vv_vector[10];
    rib_results[1].M_h1_pv = M_h1_pv_vector[10];
    rib_results[1].M_h2_vv = M_h2_vv_vector[10];
    rib_results[1].M_h2_pv = M_h2_pv_vector[10];
    rib_results[1].Q_vv = Q_vv_vector[10];
    rib_results[1].Q_pv = Q_pv_vector[10];
    rib_results[1].M_pd = M_pd_vector[10];
    rib_results[1].M_vd = M_vd_vector[10];
    rib_results[1].koef_n = 0.96;

    rib_results[2].M_h1_vv = M_h1_vv_vector[14];
    rib_results[2].M_h1_pv = M_h1_pv_vector[14];
    rib_results[2].M_h2_vv = M_h2_vv_vector[14];
    rib_results[2].M_h2_pv = M_h2_pv_vector[14];
    rib_results[2].Q_vv = Q_vv_vector[14];
    rib_results[2].Q_pv = Q_pv_vector[14];
    rib_results[2].M_vd = M_vd_vector[14];
    rib_results[2].M_pd = M_pd_vector[14];
    rib_results[2].koef_n = 0.96;
}

void calc_class::get_dynamic_rib_results(
                                    std::vector<double> Q_vd_vector
                                  , std::vector<double> Q_pd_vector
                                  )
{

    rib_results[0].Q_vd = Q_vd_vector[4];
    rib_results[0].Q_pd = Q_pd_vector[4];

    rib_results[1].Q_vd = Q_vd_vector[10];
    rib_results[1].Q_pd = Q_pd_vector[10];


    rib_results[2].Q_vd = Q_vd_vector[14];
    rib_results[2].Q_pd = Q_pd_vector[14];
}

void calc_class::get_damaged_external_forces(
                                 const std::vector<double>& M_h2_vv_vector
                               , const std::vector<double>& M_h2_pv_vector
                               , const std::vector<double>& M_vd_vector
                               , const std::vector<double>& M_pd_vector
                               )
{
    rib_results[0].ext_forces.M_h2_vv = M_h2_vv_vector[4];
    rib_results[0].ext_forces.M_h2_pv = M_h2_pv_vector[4];
    rib_results[0].ext_forces.M_vd    = M_vd_vector[4];
    rib_results[0].ext_forces.M_pd    = M_pd_vector[4];

    rib_results[1].ext_forces.M_h2_vv = M_h2_vv_vector[10];
    rib_results[1].ext_forces.M_h2_pv = M_h2_pv_vector[10];
    rib_results[1].ext_forces.M_vd    = M_vd_vector[10];
    rib_results[1].ext_forces.M_pd    = M_pd_vector[10];

    rib_results[2].ext_forces.M_h2_vv = M_h2_vv_vector[14];
    rib_results[2].ext_forces.M_h2_pv = M_h2_pv_vector[14];
    rib_results[2].ext_forces.M_vd    = M_vd_vector[14];
    rib_results[2].ext_forces.M_pd    = M_pd_vector[14];
}

void calc_class::get_sigmas()
{
    rib_results[0].Sigma_dn_kr = 3200.;  /// Сигма - днища krитическая
    rib_results[0].Sigma_dn_pr = 3000.;  /// Сигма - Pалубы _rитическая

    rib_results[1].Sigma_dn_kr = 3200.;
    rib_results[1].Sigma_dn_pr = 3000.;

    rib_results[2].Sigma_dn_kr = 3200.;
    rib_results[2].Sigma_dn_pr = 3000.;
}

QString calc_class::calc_ribs_connections()
{
    ribs_holder r_h(rib_results);

    QString exploded;
    if (inputs.explosion_sphere.size())
    {
        std::vector<sphere> explosions;
        for (size_t i = 0; i < inputs.explosion_sphere.size(); i+=4)
        {
            sphere explosion(inputs.explosion_sphere[i]
                           , inputs.explosion_sphere[i+1]
                           , inputs.explosion_sphere[i+2]
                           , inputs.explosion_sphere[i+3]
                           );
            explosions.push_back(explosion);
        }
        exploded = r_h.explode(explosions);
    }
    QString not_excluded = r_h.exclude(inputs.excluded_elements);
    rh_copy = r_h;

    for (size_t i=0; i < r_h.frames.size(); ++i)
    {
        r_h.calc_rib(i);
        if (r_h.frames[i].damaged)
            get_nzp_combat_min(r_h.frames[i].n_zp.get_n_combat_min());
        else
            get_nzp_min(r_h.frames[i].n_zp.get_n_m_min()
                      , r_h.frames[i].n_zp.get_n_combat_min()
                      );
    }
    return exploded;
    //return not_excluded;
}

void calc_class::calc_ribs_connections(bool diag)
{
    rh_copy.update(rib_results);

    for (size_t i=0; i < rh_copy.frames.size(); ++i)
    {
        rh_copy.calc_rib(i);
        if (rh_copy.frames[i].damaged)
            get_nzp_combat_min(rh_copy.frames[i].n_zp.get_n_combat_min());
        else
            get_nzp_min(rh_copy.frames[i].n_zp.get_n_m_min()
                      , rh_copy.frames[i].n_zp.get_n_combat_min()
                      );
    }
}

void calc_class::get_nzp_min(double n_m_min, double n_q_min)
{
    result.n_m_zp_min_all.push_back(std::min(n_m_min, n_q_min));
    if (cmpr::is_zero(result.n_m_zp_min))
    {
        result.check_n_m_min(n_m_min);
        result.check_n_q_min(n_q_min);
        log << "n_m chosen : "
                  << result.n_m_zp_min;
        log << "n_q chosen : "
                  << result.n_q_zp_min ;
    }
    else
    {
        result.n_m_zp_min = std::min(n_m_min
                                   , result.n_m_zp_min
                                   );
        result.n_q_zp_min = std::min(n_q_min
                                   , result.n_q_zp_min
                                   );
    }
}

void calc_class::get_nzp_combat_min(double n_combat_min)
{
    result.n_m_zp_min_all.push_back(n_combat_min);
    if (cmpr::is_zero(result.n_m_zp_min))
    {
        result.check_n_q_min(n_combat_min);
        log << "n_combat chosen : "
                  << result.n_q_zp_min ;
    }
    else
        result.n_q_zp_min = std::min(n_combat_min
                                   , result.n_q_zp_min
                                   );
}

void calc_class::prepare(const char* file_name)
{
    for (variables::var_map_iterator it = vars.var_map.begin()
                              ; it != vars.var_map.end()
                              ; ++it
                              )
        it->second.flush();

    vars.get_from_xml(file_name);
    vars.overwrite(inputs.input_vars);
    log << vars_copy.var_map["M_koef"] ;

    result.flush();
}

void calc_class::prepare(const int step)
{
    if (step)
    {
        vars = vars_copy;
        log << "vars.size() = " << vars.var_map.size();
        log << "vars_copy.size() = " << vars_copy.var_map.size();
        log << "copying data for next iteration, size: " << vars.var_map.size();
        log << vars.var_map["M_koef"];
        result.flush();
    }
    else
        vars_copy = vars;
}
