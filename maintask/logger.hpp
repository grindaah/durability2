#ifndef __LOGGER_HPP__
#  define __LOGGER_HPP__

#  include <iostream>

enum log_level_t { log_error, log_warning, log_trace, log_info, log_debug };
static const char* const buffer[] = {"error","warning","trace","info","debug"};

const log_level_t common_level = log_debug;

class logger
{
public:
    logger( log_level_t l = log_debug);
    virtual ~logger( void );

    //std::ostringstream& get_msg(log_level_t level = log_info);
    // возвращает уровень детализации
    log_level_t reporting_level();
    void set_level(log_level_t);

    static const char* leveltostring(log_level_t level);
    static log_level_t levelfromstring(const char* ch_level);

protected:
    //std::ostringstream m_oss;
private:
    log_level_t m_loglevel;
    log_level_t m_oldlevel;

    logger(const logger&);
    logger& operator = (const logger&);
};

template <typename T>
logger& operator <<(logger& log, T const& value)
{
    //if (log.reporting_level() < common_level)
        std::cout << "(" << log.leveltostring(log.reporting_level()) << "):" << value << std::endl;
    return log;
}

/*class out_stream {
public:
    // инициализирует(пересоздавая) файл лога
    //static bool init_log_file(const char* nameFileLog);
    // перенаправление вывода сообщений в файл
    //static bool use_file( void );
    // перенаправление вывода сообщений в консоль
    static bool use_stdout( void );
    // надо ли выводить время события в лог
    //static bool& use_outputtime( void );
};

class log : public logger < out_stream > { };*/

#endif // __LOGGER_HPP__
