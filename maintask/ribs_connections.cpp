#include "ribs_connections.hpp"
#include <stdio.h>
#include <sstream>
#include <algorithm>

void ribs_holder::set_M_tv()
{
    ///Моменты на тихой воде соответствующие каждый своему теоретическому шпангоуту
    M_tv.reserve(3);
    Q_tv.reserve(3);
    t_sum.reserve(3);
    X.reserve(3);
    X_zone.reserve(3);
    delta_Z.reserve(3);
    M_tv[0] = 3108.;
    M_tv[1] = 7480.;
    M_tv[2] = 5922.;
    ///Перерезывающие силы на тихой воде
    Q_tv[0] = 49.;
    Q_tv[1] = 8.5;
    Q_tv[2] = -14.;

    t_sum[0] = 0.8;
    t_sum[1] = 0.8;
    t_sum[2] = 0.8;

    X[0] = 55.38;
    X[1] = 33.18;
    X[2] = 1.3;
//    X[0] = 61.8;
//    X[1] = 39.6;
//    X[2] = 7.2;

    X_zone[0] = X[0] + 32.3;
    X_zone[1] = X[1] + 22.2;
    X_zone[2] = X[2] + 31.68;

    delta_Z[0] = 5.0;
    delta_Z[1] = 5.0;
    delta_Z[2] = 5.0;
}

ribs_holder::ribs_holder(std::vector<rib_frame_inputs> rez)
    : frame_inputs(rez)
    , M_tv(3)
    , Q_tv(3)
    , t_sum(3)
    , X(3)
    , X_zone(3)
    , delta_Z(3)
{
    ///Заполнение массива моментов на тихой воде
    set_M_tv();

    for (int i = 0; i < 3; ++i)
    {
        ribframe rib1;
        ///setting the inputs from previous parts and constants
        rib1.frame_inputs = rez[i];
        rib1.M_tv = M_tv[i];
        rib1.Q_tv = Q_tv[i];
        rib1.t_sum = t_sum[i];
        rib1.X = X[i];
        rib1.X_zone = X_zone[i];
        rib1.Z_diff = delta_Z[i];

        char file_name[9];
        snprintf(file_name, 9, "bir%d.xml", i+1);

        pugi::xml_document doc;
        pugi::xml_parse_result result = doc.load_file(file_name);
        doc.set_name(file_name);

        if (result)
            ///\warning modifying ribframe inside this function
            imbue_rib_data(doc, rib1);
        else
            xml_exception_thrower(result);

        snprintf(file_name, 9, "rib%d.xml", i+1);
        pugi::xml_document right_board_doc;
        result = right_board_doc.load_file(file_name);
        if (result)
        {
            ///\warning modifying ribframe inside this function
            imbue_rib_data(right_board_doc, rib1);
        }
        else
            xml_exception_thrower(result);

        frames.push_back(rib1);
    }
}

void ribs_holder::update(std::vector<rib_frame_inputs> rez)
{
    for (int i = 0; i < 3; ++i)
        frames[i].frame_inputs = rez[i];
}

///\warning modifying ribframe inside this function
void ribs_holder::imbue_rib_data(const pugi::xml_document& doc, ribframe& frame)
{
    constants_holder<std::string> ids;

    var.var_map["F_i"].flush();
    var.var_map["Z_i"].flush();
    var.var_map["Y_i"].flush();

    std::cerr << "rib class created, going to read XML file:" << doc.name() << std::endl;
    pugi::xml_node tools = doc.root().first_child();
    for (pugi::xml_node tool = tools.first_child(); tool; tool = tool.next_sibling())
    {
        var_base::var_map_iterator it = var.var_map.find( std::string(tool.name()));
        std::cerr << "searching: " << tool.name() << std::endl;
        if (it != var.var_map.end())
        {
            it->second >> tool.first_attribute().value();
            //std::cout << "Constant from XML:" << it->second
            //          << "size: " << it->second.size()
            //          << std::endl;
        }
        else if (std::string(tool.name()) == std::string("ids"))
        {
            ids >> tool.first_attribute().value();
            ids.setname("ids");
            //std::cout << "Constant from XML: " << ids <<  std::endl;
        }
    }
    if (var.var_map["F_i"].size() != var.var_map["Z_i"].size()
         || ids.size() != var.var_map["Z_i"].size()
         || ids.size() != var.var_map["Y_i"].size()
       )
    {
        std::cerr << "sizes unequal: \nF_i: "
                     << var.var_map["F_i"].size()
                     << "\nZ_i: " << var.var_map["Z_i"].size()
                     << "\nY_i: " << var.var_map["Y_i"].size()
                     << "\nids : " << ids.size() << std::endl;
        std::cerr << doc.root().first_child().first_attribute().value();
        d_exception ex(d_exception::invalid_input_data);
        ex.addition = doc.root().first_child().first_attribute().value();
        throw ex;
    }

    for (size_t i = 0; i < var.var_map["F_i"].size(); ++i)
        frame.ribs_map[ids.at(i)] = rib(ids.at(i)
                           , var.var_map["F_i"].at(i)
                        ///\todo don't forget XYcoordinates
                           , frame.X
                           , var.var_map["Y_i"].at(i)
                           , var.var_map["Z_i"].at(i)
                          );
}

void ribs_holder::calc_rib(int index)
{
    std::cout << "Preparing data for rib frame number " << index << std::endl;
    frames[index].calc();
    frames[index].print_all();
}

void ribs_holder::xml_exception_thrower(const pugi::xml_parse_result& result)
{
    char the_current_path[255];
    if (!getcwd(the_current_path, sizeof(the_current_path)))
    {
        the_current_path[0] = '\\';
    }

    the_current_path[sizeof(the_current_path) - 1] = '\0';

    d_exception ex(result, the_current_path);
    throw ex;
}

QString ribs_holder::explode(const std::vector<sphere>& explosions)
{
    std::set<std::string> ids_copy;

    std::cout << "Explosion: " << std::endl;

    for (size_t i = 0; i < explosions.size(); ++i)
    {
        std::set<std::string> exploded_set = calc_explosion(explosions[i]);
        ids_copy.insert(exploded_set.begin(), exploded_set.end());
    }

    std::stringstream ss;

    ///erasing empty string which will come from functor
    ids_copy.erase(std::string());
    if (ids_copy.size())
    {
        ss << "Elems=[";

        for (std::set<std::string>::iterator it = ids_copy.begin(); it != ids_copy.end(); ++it)
            if (it->size())
            {
                if (it != ids_copy.begin())
                    ss << ", " << *it;
                else
                    ss << *it;
            }
    }
    else
        return QString();

    ss << "]";

    QString result(ss.str().c_str());
    return result;
}

std::set<std::string> ribs_holder::calc_explosion(const sphere& explosion)
{
    std::set<std::string> ids_copy;

    for (size_t i = 0; i < frames.size(); ++i)
    {
        if (explosion.x >= frames[i].X && explosion.x <= frames[i].X_zone)
        {
            std::transform(
                frames[i].ribs_map.begin()
              , frames[i].ribs_map.end()
              , std::inserter(ids_copy, ids_copy.begin())
              , damage_projection_checker(explosion, frames[i].Z_diff)
              );
            explode_frame(i, ids_copy);
        }
        /*else if (explosion.x > frames[i].X)
        {
            if(explosion.x - explosion.r <= frames[i].X)
            {
                std::transform(
                    frames[i].ribs_map.begin()
                  , frames[i].ribs_map.end()
                  , std::inserter(ids_copy, ids_copy.begin())
                  , damage_sphere_checker(explosion, frames[i].Z_diff)
                  );
                explode_frame(i, ids_copy);
            }
        }
        else if (explosion.x < frames[i].X)
        {
            if(explosion.x + explosion.r >= frames[i].X)
            {
                std::transform(
                    frames[i].ribs_map.begin()
                  , frames[i].ribs_map.end()
                  , std::inserter(ids_copy, ids_copy.begin())
                  , damage_sphere_checker(explosion, frames[i].Z_diff)
                  );
                explode_frame(i, ids_copy);
            }
        }*/
    }

    return ids_copy;
}

void ribs_holder::exclude_frame(size_t frame_no, std::set<std::string>& ids)
{
    for (std::set<std::string>::iterator it = ids.begin(); it != ids.end(); ++it)
    {
        if (frames[frame_no].exclude(*it))
        {
            std::cout << *it << std::endl;
            ids.erase(*it);
        }
    }
}

void ribs_holder::explode_frame(size_t frame_no, const std::set<std::string>& ids)
{
    for (std::set<std::string>::iterator it = ids.begin(); it != ids.end(); ++it)
        if (frames[frame_no].exclude(*it))
            std::cout << *it << std::endl;
}

QString ribs_holder::exclude(const std::set<std::string> &ids)
{
    std::set<std::string> ids_copy(ids);
    for (size_t i = 0; i < frames.size(); ++i)
    {
        exclude_frame(i, ids_copy);
    }

    std::stringstream ss;

    if (ids_copy.size())
    {
        ss << "Elems=[";

        std::cout << "warning, elements wasn't parsed: ";
        for (std::set<std::string>::iterator it = ids_copy.begin(); it != ids_copy.end(); ++it)
            if (it != ids_copy.begin())
                ss << ", " << *it;
            else
                ss << *it;
    }
    else
        return QString();

    QString result(ss.str().c_str());
    return result;
}
