/**
 * \file
 * \brief  Functions comparation real numbers with fixed precision declaration
 *
 *
 */

#ifndef __COMPARE_HPP__
#  define __COMPARE_HPP__

#include <cmath>

namespace cmpr
{

const double DBL_BASE        = 0.000001;

/// precision for distances
const double DBL_DISTANCE    = 0.01;
const double DBL_DISTANCE_KM = 0.00001;
const double DBL_DISTANCE_M  = 0.01;

/// precision for angles
const double DBL_ANGLE       = 0.001;
const double DBL_ANGLE_DEG   = 0.00001;
const double DBL_ANGLE_RDN   = 0.00000001; //1,7453292e-7

///\todo think will i need this class?
/*struct double_holder
{
    double val;

    double_holder(double v) : val(v)
    {}

    bool operator <(double rhv)
    {
        return cmpr::less(val, rhv);
    }
};

class interval_checker
{
    std::set<double_holder> intervals;

    interval_checker()
    {
        intervals.insert(double_holder(3.5));
        intervals.insert(double_holder(3.75));
        intervals.insert(double_holder(4.0));
        intervals.insert(double_holder(4.25));
        intervals.insert(double_holder(4.5));
        intervals.insert(double_holder(4.75));
        intervals.insert(double_holder(5.0));
        intervals.insert(double_holder(5.25));
        intervals.insert(double_holder(5.5));
    }

    bool get_interval(const double value
                      , std::string* str1
                      , std::string* str2
                      , double diff
                      )
    {

    }
};*/

/**
 * @brief compare value and 0.0 with fixed precision declaration
 *
 * @return boolean value
 * false if v != 0.0
 * true  if v == 0.0
 */
template <typename T>
inline bool is_zero(const T v, const double eps = DBL_BASE)
{
    return std::fabs(v) < eps;
}

/**
 * @brief compare value and 0.0 with fixed precision declaration
 *
 * @return boolean value
 * false if v == 0.0
 * true  if v != 0.0
 */
template <typename T>
inline bool not_zero(const T v, const double eps = DBL_BASE)
{
    return std::fabs(v) >= eps;
}

/**
 * @brief compare two values for equal with fixed precision declaration
 *
 * @return boolean value
 * false if v1 != v2
 * true  if v1 == v2
 */
template <typename T>
inline bool equal(const T v1, const T v2, const double eps = DBL_BASE)
{
    return std::fabs(v1 - v2) < eps;
}

/**
 * @brief compare two values with fixed precision declaration на тот факт, что число v1 > v2
 *
 * @return boolean value
 * false if v1 <= v2
 * true  if v1 > v2
 */
template <typename T>
inline bool greater(const T v1, const T v2, const double eps = DBL_BASE)
{
    return v1 - v2 >= eps;
}

/**
 * @brief compare two values with fixed precision declaration на тот факт, что число v1 < v2
 *
 * @return boolean value
 * false if v1 >= v2
 * true  if v1 < v2
 */
template <typename T>
inline bool less(const T v1, const T v2, const double eps = DBL_BASE)
{
    return v2 - v1 >= eps;
}

/**
 * @brief compare two values with fixed precision declaration на тот факт, что число v1 >= v2
 *
 * @return boolean value
 * false if v1 < v2
 * true  if v1 >= v2
 */
template <typename T>
inline bool greater_or_equal(const T v1, const T v2, const double eps = DBL_BASE)
{
    return (v1 - v2 >= eps) || (std::fabs(v1 - v2) < eps);
}

/**
 * @brief compare two values with fixed precision declaration на тот факт, что число v1 <= v2
 *
 * @return boolean value
 * false if v1 > v2
 * true  if v1 <= v2
 */
template <typename T>
inline bool less_or_equal(const T v1, const T v2, const double eps = DBL_BASE)
{
    return (v2 - v1 >= eps) || (std::fabs(v1 - v2) < eps);
}

/**
 * @brief compare two values with fixed precision declaration на тот факт, что число v1 <> v2
 *
 * @return boolean value
 * false if v1 == v2
 * true  if v1 <> v2
 */
template <typename T>
inline bool greater_or_less(const T v1, const T v2, const double eps = DBL_BASE)
{
    return std::fabs(v1 - v2) >= eps;
}

/**
 * @brief compare two values with fixed precision declaration на тот факт, что число v1 != v2
 *
 * @return boolean value
 * false if v1 == v2
 * true  if v1 <> v2
 */
template <typename T>
inline bool not_equal(const T v1, const T v2, const double eps = DBL_BASE)
{
    return std::fabs(v1 - v2) >= eps;
}

/**
 * @brief summ two values and compare sum and 0.0 with fixed precision declaration
 *
 * @return boolean value
 * sum = v1 + v2, res = false, if v1 + v2 != 0.0
 * sum = 0.0, res = true, if v1 + v2 == 0.0
 */
template <typename T>
inline bool sum_and_check(const T v1, const T v2, T& sum, const double eps = DBL_BASE)
{
    sum = v1 + v2;
    if (std::fabs(sum) < eps)
    {
        sum = 0.0;
        return true;
    };
    return false;
}

/**
 * @brief compare value and 0.0 with fixed precision declaration
 *
 * @return T value
 * v   if v != 0.0
 * 0.0 if v == 0.0
 */
template <typename T>
inline double validate_zero(const T v, const T eps = DBL_BASE)
{
    return std::fabs(v) < eps ? 0.0 : v;
}

/**
 * @brief check value for INF & NaN
 *
 * @return boolean value
 * true  if -INF <v < INF и v != NaN
 * false if NaN or INF
 */
template <typename T>
inline bool not_nan_and_inf(const T v)
{
   return finite(v) != 0;
}

}// namespace math

#endif // __COMPARE_HPP__
