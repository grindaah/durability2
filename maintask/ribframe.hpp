#ifndef __RIBFRAME_HPP__
#  define __RIBFRAME_HPP__

#  include <map>
#  include <string>
#  include <iostream>
#  include <math.h>
#  include "external_forces.hpp"
#  include "compare.hpp"


struct rib_frame_inputs
{
    double M_h1_vv;
    double M_h1_pv;
    double M_h2_vv;
    double M_h2_pv;
    double M_vd;
    double M_pd;

    double Q_vv;
    double Q_pv;
    double Q_vd;
    double Q_pd;

    double Sigma_dn_kr;                                     ///< критические напряжения при сжатии днища
    double Sigma_dn_pr;                                     ///< критические напряжения при сжатии палубы

    double koef_n;

    external_forces ext_forces;
};

/**
 * \struct rib
 * \brief  struct holder for ribs data
 *
 */
struct rib
{
    std::string  id;
    double F_i;                                            ///< Площадь поперечностного сечения
    double x, y;                                           ///< XY-Координаты для расчета повреждений
    double Z_i;                                            ///< Отстояние центра площади сечения
    double S_i;                                            ///< Статический момент
    double J_i;                                            ///< Переносный момент инерции

    rib()
    {
    }

    rib(const std::string& _id, double _Fi, double _Zi) :
        id(_id)
      , F_i(_Fi)
      , x(0.)
      , y(0.)
      , Z_i(_Zi)
    {
        S_i = F_i * Z_i;
        J_i = S_i * Z_i;
    }

    rib(const std::string& _id
        , double _Fi
        , double _X
        , double _Y
        , double _Zi
        ) :
        id(_id)
      , F_i(_Fi)
      , x(_X)
      , y(_Y)
      , Z_i(_Zi)
    {
        S_i = F_i * Z_i;
        J_i = S_i * Z_i;
    }

    rib& operator+ (const rib& rhv)
    {
        this->F_i = F_i + rhv.F_i;
        this->Z_i = Z_i + rhv.Z_i;
        this->S_i = S_i + rhv.S_i;
        this->J_i = J_i + rhv.J_i;
        return *this;
    }

    friend std::ostream& operator <<(std::ostream& str, const rib& rhv)
    {
        return str << "\nID:"    << rhv.id
                   << "\nF_i = " << rhv.F_i
                   << "\nZ_i = " << rhv.Z_i
                   << "\nS_i = " << rhv.S_i
                   << "\nJ_i = " << rhv.J_i
                   << "\n(" << rhv.x << "," << rhv.y << "," << rhv.Z_i << ")"
                   << std::endl;
    }
};


/**
 * \struct factor_of_safety
 * \brief  just to hold and give output for safety data
 *
 */
struct factor_of_safety
{
    double n_zp_up;
    double n_zp_down;

    double n_zp_up_combat;
    double n_zp_down_combat;

    friend std::ostream& operator <<(std::ostream& str, const factor_of_safety& rhv)
    {
        return str << "\nn_zp1 = "     << rhv.n_zp_down
                   << "\nn_zp1_rbi = " << rhv.n_zp_down_combat
                   << "\nn_zp2 = "     << rhv.n_zp_up
                   << "\nn_zp2_rbi = " << rhv.n_zp_up_combat
                   << std::endl;
    }

    void check_critical_damage()
    {
        if (cmpr::less(n_zp_up, 0.8))
            n_zp_up = 0.;
        if (cmpr::less(n_zp_up_combat, 0.8))
            n_zp_up_combat = 0.;
        if (cmpr::less(n_zp_down, 0.8))
            n_zp_down = 0.;
        if (cmpr::less(n_zp_down_combat, 0.8))
            n_zp_down_combat = 0.;
    }

    double get_n_m_min()
    {
        return std::min(fabs(n_zp_up), fabs(n_zp_down));
    }
    double get_n_combat_min()
    {
        return std::min(fabs(n_zp_up_combat), fabs(n_zp_down_combat));
    }
};

struct destr_forces
{
    double Q_rasch_up;
    double Q_rasch_down;

    double Q_rasch_up_combat;
    double Q_rasch_down_combat;
};

struct sphere
{
    sphere(double _x
         , double _y
         , double _z
         , double _r
         )
        :
          x(_x)
        , y(_y)
        , z(_z)
        , r(_r)
    {}

    double x,y,z,r;
};

class damage_projection_checker : public std::unary_function<
    const std::pair<std::string, rib>& , bool >
{
public:
    damage_projection_checker(const sphere& dmg_sphere
                            , const double _z_diff
                            ) : m_sphere(dmg_sphere)
                              , z_diff(_z_diff)
    {}

    std::string operator()(const std::pair<std::string, rib>& elem)
    {
        double y = elem.second.y - m_sphere.y;
        double z = elem.second.Z_i + z_diff - m_sphere.z;
        double r1 = sqrt(y*y + z*z);
        return r1 < m_sphere.r ? elem.second.id : std::string();
    }

private:
    sphere m_sphere;
    double z_diff;
};

class damage_sphere_checker : public std::unary_function<
    const std::pair<std::string, rib>& , bool >
{
public:
    damage_sphere_checker(const sphere& dmg_sphere
                        , const double _z_diff
                        ) : m_sphere(dmg_sphere)
                          , z_diff(_z_diff)
    {}

    std::string operator()(const std::pair<std::string, rib>& elem)
    {
        double x = elem.second.x - m_sphere.x;
        double y = elem.second.y - m_sphere.y;
        double z = elem.second.Z_i + z_diff - m_sphere.z;
        double r1 = sqrt(x*x
                       + y*y
                       + z*z
                       );
        return r1 < m_sphere.r ? elem.second.id : std::string();
    }

private:
    sphere m_sphere;
    double z_diff;
};

struct ribframe
{
    ribframe() :
      F_i_sum(0.)
    , S_i_sum(0.)
    , Z_0_i(0.)
    , Z_i_sum(0.)
    , Z_0_i_sum(0.)
    , Z_0_max(0.)
    , Z_0_min(0.)
    , J_i_sum(0.)
    , I(0.)
    , W_p(0.)
    , W_dn(0.)
    , W(0.)
    , M_v_pr(0.)
    , M_p_pr(0.)
    , M_tv(0.)
    , Q_tv(0.)
    , S_star(0.)
    , tau_e(0.)
    , t_sum(0.)
    , X(0.)
    , X_zone(0.)
    , Z_diff(0.)
    , damaged(false)
    {

    }

    typedef std::map<std::string, rib>::iterator ribelem_iterator;
    std::map<std::string, rib> ribs_map;

    double F_i_sum;
    double S_i_sum;
    double Z_0_i;
    double Z_i_sum;
    double Z_0_i_sum;
    double Z_0_max;
    double Z_0_min;
    double J_i_sum;

    double I;                                              ///< центральный момент инерции

    double W_p, W_dn;
    double W;                                              ///< минимальный момент сопротивления

    double M_v_pr, M_p_pr;

    factor_of_safety n_zp;
    destr_forces     Q_rasch;

    double M_tv, Q_tv;

    double S_star;
    double tau_e;                                          ///< эйлеровы касательные напряжения

    double t_sum;                                          ///суммарная толщина бортов
    double X;
    double X_zone;
    double Z_diff;
    rib_frame_inputs frame_inputs;
    bool damaged;

    rib Z_i_min_rib;
    rib Z_i_max_rib;

    void calc();
    void print_all();
    void flush();
    bool exclude(const std::string elem);
};


#endif //__RIBFRAME_HPP__

