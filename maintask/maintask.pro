TEMPLATE = lib
TARGET = maintask
CONFIG += staticlib

LIBS += ../pugixml/libpugixml.a

SOURCES = calc_class.cpp \
    interpolate.cpp \
    ribs_connections.cpp \
    ribframe.cpp \
    d_exceptions.cpp \
    logger.cpp

HEADERS += constants_holder.hpp\
           calc_class.hpp \
           interpolate.hpp \
    compare.hpp \
    d_exceptions.hpp \
    ribs_connections.hpp \
    task_results.hpp \
    input_interface.hpp \
    ribframe.hpp \
    string_trimming.hpp \
    external_forces.hpp \
    logger.hpp
 
