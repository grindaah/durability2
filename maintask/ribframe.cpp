#include "ribframe.hpp"
#include "compare.hpp"
#include <stdlib.h>

void ribframe::calc()
{
    flush();

    if (ribs_map.size() < 3)
    {
        n_zp.n_zp_up = 0.;
        n_zp.n_zp_down = 0.;
        n_zp.n_zp_up_combat = 0.;
        n_zp.n_zp_down_combat = 0.;
        n_zp.check_critical_damage();
        return;
    }

    std::map<std::string, rib>::iterator it;

    for (it = ribs_map.begin(); it != ribs_map.end(); ++it)
    {
        F_i_sum += it->second.F_i;
        S_i_sum += it->second.S_i;
        J_i_sum += it->second.J_i;
    }

    Z_i_sum = S_i_sum / F_i_sum;
    Z_0_min = ribs_map.begin()->second.Z_i - Z_i_sum;
    Z_0_max = ribs_map.begin()->second.Z_i - Z_i_sum;

    Z_i_min_rib = ribs_map.begin()->second;
    Z_i_max_rib = ribs_map.begin()->second;

    //Z_0_i_sum = ribs_map.begin()->second.Z_i;

    ///\note cycle starting from the second element
    for (it = ribs_map.begin(); it != ribs_map.end(); ++it)
    {
        ///skip first!
        if (it != ribs_map.begin())
        {
            Z_0_i =  it->second.Z_i  - Z_i_sum;

            if (cmpr::less(Z_0_i, Z_0_min))
            {
                Z_0_min = std::min(Z_0_min, Z_0_i);//Z_0_i);
                Z_i_min_rib = it->second;
            }

            if(cmpr::greater(Z_0_i, Z_0_max))
            {
                Z_0_max = std::max(Z_0_max, Z_0_i);
                Z_i_max_rib = it->second;
            }
            //Z_0_i_sum += it->second.Z_i;//Z_0_i;
        }
    }


    I = (J_i_sum - (S_i_sum * Z_i_sum) );

    W_p = I / fabs(Z_0_max);
    W_dn = I / fabs(Z_0_min);

    W = std::min(W_p, W_dn);

    M_v_pr = W * frame_inputs.Sigma_dn_kr /1000.;
    M_p_pr = W * frame_inputs.Sigma_dn_pr /1000.;


    n_zp.n_zp_up = frame_inputs.koef_n * M_v_pr / (M_tv + 1.2*fabs(frame_inputs.M_h1_vv));
    n_zp.n_zp_down = frame_inputs.koef_n * M_p_pr / (M_tv + 1.2*fabs(frame_inputs.M_h1_pv));
    if (!damaged)
    {

        n_zp.n_zp_up_combat = frame_inputs.koef_n * M_v_pr / (M_tv + frame_inputs.M_h2_vv + frame_inputs.M_vd);
        n_zp.n_zp_down_combat = frame_inputs.koef_n * M_p_pr
                / (M_tv + fabs(frame_inputs.M_h2_pv) + fabs(frame_inputs.M_pd));
    }
    else
    {
        n_zp.n_zp_up_combat = frame_inputs.koef_n * M_v_pr / (M_tv + frame_inputs.ext_forces.M_h2_vv + frame_inputs.ext_forces.M_vd);
        n_zp.n_zp_down_combat = frame_inputs.koef_n * M_p_pr
                / (M_tv + fabs(frame_inputs.ext_forces.M_h2_pv) + fabs(frame_inputs.ext_forces.M_pd));
    }

    n_zp.check_critical_damage();

    Q_rasch.Q_rasch_up = fabs(Q_tv + frame_inputs.Q_vv);
    Q_rasch.Q_rasch_up_combat = fabs(Q_tv + frame_inputs.Q_vv + frame_inputs.Q_vd);
    Q_rasch.Q_rasch_down = fabs(Q_tv + frame_inputs.Q_pv);
    Q_rasch.Q_rasch_down_combat = fabs(Q_tv + frame_inputs.Q_pv + frame_inputs.Q_pd);

    S_star = 2 * (Z_0_i_sum * F_i_sum);
    tau_e = 0.57 * 4000.0;//4000 - sigma_t - предел текучести
}

void ribframe::flush()
{
    F_i_sum = 0.;
    S_i_sum = 0.;
    J_i_sum = 0.;
}

bool ribframe::exclude(const std::string elem)
{
    if (ribs_map.erase(elem))
    {
        damaged = true;
        return true;
    }
    return false;
}

void ribframe::print_all()
{
    std::cout << "Rib summary: ==================="
              << "\nF_i_sum = " << F_i_sum
              << "\nS_i_sum = " << S_i_sum
              << "\nZ_0_min = " << Z_0_min
              << "\nZ_0_max = " << Z_0_max
              << "\nZ_i_sum = " << Z_i_sum
              << "\nJ_i_sum = " << J_i_sum
              << "\nZ_0_min :" << Z_i_min_rib
              << "\nZ_0_max :" << Z_i_max_rib
              << "\nW = "       << W
              << "\nWdn = "     << W_dn
              << "\nWp = "      << W_p
              << "\nI = "       << I
              << "\nM_v_pr = " << M_v_pr
              << "\nM_p_pr = " << M_p_pr
              << "\ninputs: "
              << "\nM_tv = " << M_tv
              << "\nM_h1_vv = " << frame_inputs.M_h1_vv
              << "\nM_h1_pv = " << frame_inputs.M_h1_pv
              << "\nM_h2_vv = " << frame_inputs.M_h2_vv
              << "\nM_h2_pv = " << frame_inputs.M_h2_pv
              << "\nkoef_n = " << frame_inputs.koef_n
              << "\nM_dv = " << frame_inputs.M_vd
              << "\nM_dp = " << frame_inputs.M_pd
              << "\n===damaged external forces==="
              << "\nM_h2_vv = " << frame_inputs.ext_forces.M_h2_vv
              << "\nM_h2_pv = " << frame_inputs.ext_forces.M_h2_pv
              << "\nM_dv = " << frame_inputs.ext_forces.M_vd
              << "\nM_dp = " << frame_inputs.ext_forces.M_pd
                 << n_zp;
}
