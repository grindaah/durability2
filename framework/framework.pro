TEMPLATE = lib
TARGET = durability18280
CONFIG += plugin debug

DEFINES += __DURABILITY_VERSION__=\\\"1.0.0\\\"

DEPENDPATH += ../maintask
LIBS += ../maintask/libmaintask.a
LIBS += ../pugixml/libpugixml.a

# Input

HEADERS += framework.hpp

SOURCES += framework.cpp
