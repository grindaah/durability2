#include <framework.hpp>
#include "../maintask/d_exceptions.hpp"


bool durability_framework::work(QString& str)
{
    try
    {
        m_calc.inputs.get_inputs(str);
        m_calc.prepare("tree.xml");
        const QString& warnings = m_calc.calculate(-1.0, -1);

        QString ss = m_calc.get_result();
        QString& ss_ref = ss;
        if (warnings.length() > 0)
            emit warningDurability(1, warnings);

        emit finished(0, ss_ref);

        return true;
    }
    catch (d_exception& ex)
    {
        QString err_str(ex.what().c_str());
        int throwing_code = ex.xml_code ? ex.xml_code : ex.code;
        emit errorDurability(throwing_code
                            , err_str
                            );
    }
    catch (int code)
    {
        emit errorDurability(code
                           , QString("Cannot obtain input data, xml not loaded")
                           );
    }
    return true;
}

bool durability_framework::calc_diagram(QString& str)
{
    log << "starting diagram";

    try
    {
        QString ss;
        m_calc.inputs.get_inputs(str);
        m_calc.prepare("tree.xml");

        constants_holder<double> cs = m_calc.inputs.input_vars.var_map["V"];

        const QString& warnings = m_calc.calculate(cs[0], -1);

        ss.append(m_calc.get_result());

        //one more prepare, because vars were flushed
        m_calc.prepare("tree.xml");

        for (int step = 0; step < 19; step++)
            for (int V = 2; V < 17; V+=2)
            {
                m_calc.prepare(step + V-2);
                m_calc.calculate(V, step);
                ss.append(m_calc.get_diag_result(V, step));
            }

        QString& ss_ref = ss;
        if (warnings.length() > 0)
            emit warningDurability(1, warnings);

        emit finished(0, ss_ref);

        return true;
    }
    catch (d_exception& ex)
    {
        QString err_str(ex.what().c_str());
        int throwing_code = ex.xml_code ? ex.xml_code : ex.code;
        emit errorDurability(throwing_code
                            , err_str
                            );
    }
    catch (int code)
    {
        emit errorDurability(code
                           , QString("Cannot obtain input data, xml not loaded")
                           );
    }
    return true;
}

QString durability_framework::constholder_as_string(const constants_holder<double>& v) const
{
    std::stringstream ss;

    ss << v.getname() << "=[";
    for (size_t i = 0; i < v.size(); ++i)
    {
        if (i < v.size()-1)
            ss << v[i] << ", ";
        else
            ss << v[i];
    }
    ss << "]";

    QString result(ss.str().c_str());
    return result;
}
