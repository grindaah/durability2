#!/usr/bin/python
#coding: utf-8


import argparse
import sys


class Element:
    def __init__(self, id, z_i, f_i, y_i):
        self.id = id
        self.z_i = z_i
        self.f_i = f_i
        self.y_i = y_i

    def __str__(self):
        return "%14s %10s %10s %10s" % (self.id, self.z_i, self.f_i, self.y_i)

    def is_frame(self, no):
        return self.id.find(no != -1)

def apply_parser(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", dest="fname", default="app/bir1.xml",
        help="name of file which you want to represent as table")
    parser.add_argument("-R", "--frame", dest="frame", default = 0,
        help="show only specific frame number")
    return parser

def validate(sz1, sz2, sz3, sz4):
    return sz1 == sz2 == sz3 == sz4

def read_file(fname):
    for line in open(fname):
        print "="*50
        if line.find("ids value") != -1:
            ids = get_quoted_text(line)
        elif line.find("Z_i value") != -1:
            zs = get_quoted_text(line)
        elif line.find("F_i value") != -1:
            fs = get_quoted_text(line)
        elif line.find("Y_i value") != -1:
            ys = get_quoted_text(line)

    if validate(ids.__len__(), zs.__len__(), fs.__len__(), ys.__len__()):
        print "Validation.... OK"
    else:
        print "Validation.... not OK, check: %d, %d, %d, %d" % (ids.__len__(), zs.__len__(), fs.__len__(), ys.__len__())

    elems = []
    for id in ids:
        index = ids.index(id)
        elems.append(Element(id, zs[index], fs[index], ys[index]))
    return elems

def get_quoted_text(ln):
    lft_quote = ln.find('"') + 1
    rt_quote = ln.rfind('"')
    print lft_quote, rt_quote
    return ln[lft_quote:rt_quote].split()

def main(argv):
    parser = apply_parser(argv)
    pr = parser.parse_args()                                # pr = parse_result
    elems = read_file(pr.fname)
    for idd in elems:
        if pr.frame == 0 or (pr.frame > 0 and idd.is_frame(pr.frame)):
            print idd

if __name__ == "__main__":
    main(sys.argv)
