#include <QtCore/QCoreApplication>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <fstream>

#include "mainframe.hpp"
#include "../pugixml/pugixml.hpp"
#include "../maintask/constants_holder.hpp"
#include "../framework/framework.hpp"

int main(int argc, char *argv[])
{
    mainframe mf;

    durability_framework* df = new durability_framework();
    durability_framework* df2 = new durability_framework();

    QObject::connect(df, SIGNAL(errorDurability(int, const QString&))
                   , &mf, SLOT(report_error(int, const QString&)) );
    QObject::connect(df, SIGNAL(finished(int, const QString&))
                   , &mf, SLOT(finished(int, const QString&) ));
    QObject::connect(df, SIGNAL(finishedDiagram(int, const QString&))
                   , &mf, SLOT(finished(int, const QString&) ));
    QObject::connect(df, SIGNAL(warningDurability(int, const QString&))
                   , &mf, SLOT(warning(int, const QString&) ));

    QObject::connect(df2, SIGNAL(errorDurability(int, const QString&))
                   , &mf, SLOT(report_error(int, const QString&)) );
    QObject::connect(df2, SIGNAL(finished(int, const QString&))
                   , &mf, SLOT(finished(int, const QString&) ));
    QObject::connect(df2, SIGNAL(finishedDiagram(int, const QString&))
                   , &mf, SLOT(finished(int, const QString&) ));
    QObject::connect(df2, SIGNAL(warningDurability(int, const QString&))
                   , &mf, SLOT(warning(int, const QString&) ));
    QObject::connect(df2, SIGNAL(sendVersion(const QString&))
                   , &mf, SLOT(show_version(const QString&) ));

    QString str("Elems=[ref_l_23_27_09,ref_r_23_34,ref_l_23_14_15,ref_l_23_17_03,ref_l_23_31_03,ref_l_23_16,ref_l_23_17_02,ref_l_23_27_07,ref_l_23_30_03,ref_l_23_27_06,ref_l_23_29_03,ref_l_23_32_03,ref_l_23_32_01,ref_l_23_17_01,ref_l_23_29_01,ref_l_23_14_14,ref_l_23_31_04,ref_l_23_30_01,ref_l_23_31_01,ref_l_23_14_12,ref_l_23_15,ref_l_23_29_02,ref_l_23_32_04,ref_l_23_14_13,ref_l_23_18_01,ref_l_23_32_02,ref_l_23_27_08,ref_l_23_17_04,ref_l_23_30_02,ref_l_23_31_02];L=69;T=2.807;V=26;D=975.;P=236.973");
    QString str2;
    if (argc > 1)
        str2 = QString().fromUtf8(argv[1]);
    else
    //    str2 = QString("Elems=[];Explosion=[33,0,3,3.6,25.91,4.44,3.77613,1];L=69;T=2.62;V=26;D=975;P=0;");
        str2 = QString("Elems=[];Explosion=[];L=86.28;T=4.96;V=16;D=3926.0;P=0;");

    QString& str_ref = str;
    //df->work(str_ref);
    std::cout << "=============================1===============================";
    //df2->work(str_ref);

    std::cout << "=============================2===============================";
    str_ref = str2;
    //df->work(str_ref);
    std::cout << "=============================3===============================";
    df2->requestVersion();
    //df2->work(str_ref);
    df2->calc_diagram(str_ref);

    delete df;
    delete df2;

    return 0;
}
