#include <QObject>
#include <QString>
#include <iostream>

#include <stdio.h>
/**
 * \class main_frame
 * \brief imitation of main project
 *
 */

class mainframe : public QObject
{
Q_OBJECT
public:
    mainframe()
    {}

public slots:
    void report_error(int code, const QString& str)
    {
//        FILE*  fLog;
//        fLog = fopen("/home/desman/durability_test/app/error.log", "w");

//        if (fLog != NULL) {
//            fprintf(fLog, "%s : \n", str.toStdString().c_str());
//            fclose(fLog);
//        }

        std::cout << "client framework reporting error: " << str.toStdString()
                  << ", code: " << code << std::endl;
    }

    void finished(int code, const QString& str)
    {
//        FILE*  fLog;
//        fLog = fopen("/home/desman/durability_test/app/success.log", "w");

//        if (fLog != NULL) {
//            fprintf(fLog, "%s : \n", str.toStdString().c_str());
//            fclose(fLog);
//        }

        std::cout << "task finished with status: " << code
                  << ", result: " << str.toStdString() << std::endl;
    }

    void warning(int code, const QString& str)
    {
        std::cout << "task received warning code: " << code
                  << ", " << str.toStdString() << std::endl;
    }

    void show_version(const QString& str)
    {
        std::cout << str.toStdString();
    }
};
